
import numpy as np
import motmetrics as mm
   
def score_tracker(o_array, h_array, ground_truth_Ids, hypotheses):
    for iframe in range(len(hypotheses)):
        print('Frame:', iframe)
        print('- - - - - - - - - - - - - - - - -')
        if iframe == 0:
            # create the accumulator for this run
            acc = mm.MOTAccumulator(auto_id=True)
            o = o_array[iframe]
            h = h_array[iframe]
            gtruth = ground_truth_Ids[iframe]
            dhyp = hypotheses[iframe]
            # compute the distances
            C = mm.distances.norm2squared_matrix(o, h, max_d2=5.0)
            # update the accumulator
            acc.update( gtruth, dhyp, C )
            print(acc.mot_events)
            print('')
        else: 
            o = o_array[iframe]
            h = h_array[iframe]
            gtruth = ground_truth_Ids[iframe]
            dhyp = hypotheses[iframe]
            # compute the distances
            C = mm.distances.norm2squared_matrix(o, h, max_d2=5.0)
            #print(gtruth)
            #print(dhyp)
            #print(C)
            # update the accumulator
            frameid = acc.update( gtruth, dhyp, C )
            print(acc.mot_events.loc[frameid])
            print('')
    
    ################################################
    # SUMMARY
    print('')
    print('---------- SUMMARY ----------')
    mh = mm.metrics.create()
    summary = mh.compute(acc, metrics=[
              'num_frames', 'mota', 'precision', 'recall', 'num_unique_objects',
              'num_matches', 'num_switches', 'num_false_positives', 'num_misses'], name='acc')
    strsummary = mm.io.render_summary(
        summary, 
        formatters={'mota' : '{:.2%}'.format}, 
        namemap={'mota': 'MOTA'}
    )
    print(strsummary)
